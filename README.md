## Adding Networking to a Watch App

In this demo, we make an app that gets the time of sunrise and sunset in a given city.


![](screenshots/demo.gif)

## How to use:

1. Download project
2. Open `APIDemo.xcworkspace`
3. Choose a watch+phone simulator
4. Run the project


## You *must* open THIS file!!!

![](screenshots/xcodeproj.png)
